module.exports = {
  devIndicators: {
    buildActivity: false
  },
  reactStrictMode: false,
  async headers() {
    return [
      {
        source: '/',
        headers: [
          {
            key: 'Cache-Control',
            value: 'public, max-age=9999999999, must-revalidate',
          },
        ],
      },
    ]
  },
  pageExtensions: ["js", "jsx", "md", "mdx"],
};
