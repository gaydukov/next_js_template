import styles from "./header.module.css"

export const Header = () => {
  return (
    <header className={styles.header}>
      <nav className={styles.nav}>
        {/** логотип*/}
        <div className={styles.logo}></div>

        {/** меню*/}
        <ul className={styles.nav_list}>
          <li></li>
        </ul>

        {/** контакты */}
        <div className={styles.contacts}></div>

      </nav>
    </header>
  )
}