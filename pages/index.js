import Meta from "../components/meta";
import {getContent} from "@/utils/getContent";

export default function Home({data}) {

  return (
    <>
      <Meta
        title={data?.seo_title || ""}
        description={data?.seo_description || ""}
        keywords={data?.seo_keywords || ""}
      >
        <link rel="canonical" href={`${process.env.NEXT_PUBLIC_WEB_SITE}`}/>
        <link rel="preconnect" href={`${process.env.NEXT_PUBLIC_WEB_SITE}`}/>
        <link rel="dns-prefetch" href={`${process.env.NEXT_PUBLIC_WEB_SITE}`} />

        <meta name="robots" content="all"/>

        <meta property="og:title" content={data?.seo_title}/>
        <meta property="og:description" content={data?.seo_description}/>
        <meta property="og:url" content={`${process.env.NEXT_PUBLIC_WEB_SITE}`}/>
        <meta property="og:type" content="website"/>
        <meta property="og:locale" content="ru_RU"/>
        <meta property="og:site_name" content="Алейск"/>
        <meta property={"og:type"} content={"website"}/>
        <meta property={"og:locale"} content={"ru_RU"}/>
        <meta property={"og:site_name"} content={"Алейск"}/>
        {data?.og_tags?.map((tag, i) => (
          <meta key={i} property={tag.property} content={tag.content}/>
        ))}
      </Meta>

    </>
  );
}

export const getStaticProps = async () => {

  const {data} = await getContent({path_file: "/content/main/index.md"});

  return {
    props: {
      data,
    }
  };

};