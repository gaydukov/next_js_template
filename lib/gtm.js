
export const pageview = (url) => {
  try {
    process.env.NEXT_PUBLIC_STAGE_DEPlOY === "production" &&
    process.env.NEXT_PUBLIC_YANDEX_ANALYTICS &&
    ym?.(process.env.NEXT_PUBLIC_YANDEX_ANALYTICS, 'hit', url);
  } catch {
    return
  }
  window?.dataLayer?.push({
    event: 'pageview',
    page: url,
  })
}
